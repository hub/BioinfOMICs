# BLAST


Use the sequence "my_mistery_protein.txt" in the "/data" directory to run a BLAST comparison to identify to which protein it corresponds.


## Identifying an unknown protein from its amino acid sequence

Q1:  What program did you choose?

Q2:  How long is your mystery sequence?

Q3: What molecule type does it have ?

Q4: What species does the first hit (WP_004920433.1) sequence belong to?

Q5: What percentage of identity do you find for this hit and your mistery protein?

Q6: What is the query coverage of this hit ? Do you think it covers all your mystery protein ?

Q7: What is the name of the mystery protein?

Q8: Search for this protein in Uniprot and check the Family and Domains section. What functions does this protein have ?


## Investigating the function of a given protein

Use the same sequence as above, but  restrict your search to the UniProt/Swissprot database.

Q9:  What program did you choose?

Q10:  What database did you search ?

Q11:  For the first hit (Accession C1DHS5.1),  what is the percentage of identity with your query?

HQ12: What is the query coverage of this hit ? Do you think it covers all your protein ?

Q13: For the second hit (Accession Q7P0E7),  what is the percentage of identity with your query?

Q14: What is the query coverage of this hit ? Do you think it covers all your protein ?

Q15: Can you identify another hit that covers better your protein ? 
     (You may  want to check the domains of the hit protein in the Uniprot DB..)
