# Assembly annotation

Use the best and the worst assembly from above, run the **Prokka** tool with the default parameters. 
In the  **additional outputs** field, select **all files**.

Browse through the files to answer the following questions:

```
1. How many contigs were given as an input to annotate? 

2. How many CDSs were found, and how many tRNAs?

3. How many repeated regions were found in the genome?

4. What is the gene product (gene description) of gene zitB?

5. What biological processes is gene zitB involved in, according to the UniprotDB ?

6. Give the gene name and gene product (gene description) of an annotated enzyme.
```


