# Genome assembly

Go to https://usegalaxy.org/ and login or register (**Top Galaxy panel**).
 

## Upload the course data to Galaxy

* Click on **Upload Data** (**Tool Galaxy panel**)
* Drag an drop data from the **raw** folder
* In the upload menu set the **data type** to fastq.gz 
* Files will be listed in the right hand panel (your current history).

For other instances, not this course, you can also upload data from NCBI, EBI, others, using the **Tool panel**.


## Check raw data quality

Perform the quality check with the **FastQC** tool. Explore the html report produced and answer:

```
1. Can you explain the reason for the warnings in the FastQC report of this dataset?
2. Imagine a different scenario, e.g. a different result where: Good quality over the whole read length but, in the nucleotide proportion panel starting from the position 80, ~100% of A (adenine) after the position 100.  What could be the reason for this behaviour?
```

## Clean raw data

Trim and filter the dataset using **Trim Galore!** and answer:

```
3. Explain your parameter choices.
```

## Run assembly

A. Use **SPAdes** genome assembler and try different settings:

* k-mer: 21-mers and 77-mers
* input data quality: raw and trimmed 

B. Use **Unicycler** genome assembler with default parameters (for the trimmed data)


## Compare assemblies 

Analyse the following table with the number of scaffolds obtained using different settings. 

```
4. Can you explain the correlation between assembly settings and result?
```

| Assembler | Input data | kmer | No scaffolds | 
|-----------|------------|------|--------------|
| SPAdes | raw | 21 |  1141 |  
| SPAdes | raw | 51 |  481 |  
| SPAdes | raw | 77 |  887 |  
| SPAdes | trimmed | 21 |  891 |  
| SPAdes | trimmed | 51 |  537 |  
| SPAdes | trimmed | 77 |  309 |  
| Unicycler | trimmed | 127 | 203|

Run the **Quast** tool on the provided assemblies and answer the following questions:

```
5. What are the differences between the assemblies done with 21-mers and 77-mers? Can you relate those differences to the k-mer size?
6. Compare the assemblies obtained with raw and trimmed reads. How is the read quality affecting the assembly done with raw and trimmed reads?
7. Compare the best SPAdes assembly and the Unicycler assembly. Which one is the best? Can you explain this result?  
```

## Visualise assemblies 

* Install the **Bandage** visualization software https://rrwick.github.io/Bandage/.
* Visualise the assembly graphs **gfa files**: File > Load graph and then Graph drawing > Draw graph


