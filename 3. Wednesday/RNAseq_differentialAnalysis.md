
# RNAseq differential analysis

## Upload data 

For this exercise you will analyse a _Mycobacterium tuberculosis_ dataset. 

The objective is to perform the differential expression analysis of BRIGHT vs DIM in each growing condition.

## Define experimental desing

* Open the **targetAll.txt** file and describe the global experimental design.


## Differential analysis with SARTools

* Open the **SARTools_TB_genes.R** script.
* Use the reduced experimental design in the **target.txt** file. 
* Complete/adapt all parameters and variables.
* Perform the differential analysis

```
1. There is an outlier sample: which one? 
2. How many up/down regulated genes are obtained for the comparisons: CB vs CD and HCB vs HCD.
```

* Filter out the outlier and repeat the analysis, without re-writing the results.

```
3. Do you see any improvement in the differential expression results? Justify your answer.
```

