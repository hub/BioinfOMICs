# RNAseq data processing

## Upload data 

For this exercise you will analyse a _Saccharomyces cerevisiae_ dataset with two conditions, wild type (WT) and mutant (KO), each one with three replicates. Follow this link to get the raw data:

https://dl.pasteur.fr/fop/eC0rt3cf/WT_KO.zip

The objective is to obtain a read counts matrix, genes x samples using two approaches:

* Map with bowtie2 in [Galaxy](https://usegalaxy.org) and count with featureCounts in R.
* Quantify with Salmon in [Galaxy](https://usegalaxy.org) 

## Map & count

### Check raw data quality

### Clean raw data

### Mapping

Use **bowtie2** to perform the mapping using: 
* Using a built-in genome (sacCer3)
* An user provided reference (GCF_000146045.2_R64_genomic.fna)   

Hint: remember to set proper Datatype: Pencil > Edit Dataset Attributes > Datatypes

```
1. How can you decide whether bowtie2 is an adequate mapping tool for this dataset? 
```
### Count

In RStudio, perform the counting using the **featureCounts** function (RNAseq_quantification.R).

```
2. What are the input files required by featureCounts? Do you always need a reference annotation? 
```


## Quantify 

Use **Salmon** in Galaxy to perform the quantification of reads over transcripts.

```
3. What are the input files needed to run Salmon?
```

In RStudio, compare the matrices obtained with the two approaches (RNAseq_quantification.R).

```
4. Are the count matrices resulting from the two approaches equivalent?  
```

