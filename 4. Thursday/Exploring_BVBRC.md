 Hands-On: Exploring BV-BRC



## Organism: Mycobacterium tuberculosis (strain erdmann)

<!-- <img src="images/ATAC-seq_PE.png" width="700" align="center" > -->

## Perfom search 

Go to https://www.patricbrc.org/

* Search for the Mycobacterium tuberculosis str. Erdman genome:
        select 652616.3
* Check the overview tab, look for:
```
Num. of prots
Num. of annotated prots with an associated  Gene Ontology 
Num. of enzymes

```
                
* Check the features tab, and change the filter options to add the GO terms
* Check all the entries and
download file (.txt)
* Put the file in the data directory

```
* Extra:
Look for virulence factor associated genes
 ... and those associated with antibiotic resistance ?
```

