
# Hands-On: OmicNet - Regulatory networks


The gene list in the data folder contains the name of genes associated with a differential dimethylation of histone 3 (H3K4me2) in fibroblasts, upon infection with Staphylococcus. We wish to find contextual information on these genes, using known interaction networks.

OmicsNet is a web-based tool to explore multi-omics data and perform visual analysis of biological networks. It can integrate new data with of high-quality molecular interaction data, such as protein–protein interactions (PPI), gene regulatory networks.

1. Upload the gene list in: https://www.omicsnet.ca/ and build your network defining a primary Interaction type.

* Which kind of interaction would you choose to:
    Explore the signalling pathways where the query genes have a major role?
    Find the transcription factors (TFs) involved in the regulation of the query genes?


2. For the second case, change the database used for building the network and calculate the network size, in terms of number of nodes and edges.

* What is the size of the network? Explain why are networks bigger or smaller depending on the interaction database.


3. Choose the interaction network for the second case, and choose the database with fewest interactions. Control the network size by trimming with e.g. Minimum Network algorithm. Proceed to the network visualization.

* What are the 5 top "hubs" of your network? 

4. Identify the network modules using the different algorithms in the Module Explorer section. 

* Are the different results robust among the different algorithms? Do they make sense in the light of the global network structure? How representative are the modules of the initial network?

5. Choose the algorithm that defines the highest number of modules and perform the functional analysis of the modules with the Function Explorer. 

* What are the significant pathways (alpha=0.05) that include more than 5 genes of the network?




