
# Hands-On: STRING DB - CANCER

Here, we will explore the intractions of genes associzated to large deletiomns in cancer.


# Input:


<!-- <img src="images/ATAC-seq_PE.png" width="700" align="center" > -->

We will use The Human Protein Atlas to find the 41 genes associated to large deletions in cancer
https://www.proteinatlas.org/

*	Choose : The tissue -> Go to the Human Subproteomes and choose The cancer proteome  -> Select the   large deletions genes

*	Download the table as a .tsv file   

*	Open in Excel and choose one column as gene identifier : copy it to clipboard


## Looking for the gene interactors

We will find our interacting proteins in  the String DB.
https://string-db.org/


*	Search -> Multiple proteins and paste list
*	Select  organism and click  the search button
*	Check the mapping, are they your prots ? How many ?
*	Click the continue button

## Exploring your network

### Check the legend tab to understand the network components

 1. What do nodes represent? 

 2. What do edges represent ?

 3. What are the figures inside the nodes ?


### Check the settings tab to change the network options


4. What types of networks can you display ?

5. What kind of interaction sources do you have in this netwrok ? 

Choose to see both functional and physical associations and that the edges represent the confidence of the data

Remove the protein structure preview

### Check the analysis tab to explore the network's main statistics

What kind of infomration can you give about this network ?

7. How many proteins does the network have ? 

8. How many interactions does the network have ?

9. How many interactions is this network expected to have if it were a random list of proteins ? 

10. How many interactions does each protein have on average ?

### Check the analysis tab to explore the functional enrichment of the network

11. Which proteins belong to the Fanconi anemia KEGG pathway ?

12. How many proteins are described in Uniprot with the Hereditary nonpolyposis colorectal cancer keyword ?

13. How many drug compounds can we find associated to the PTEN protein ?


<details>
  <summary markdown="span">14. Save a figure of the network in file</summary>
Exports -> svg

</details>

<details>
  <summary markdown="span">15. Save a table of the protein interactions </summary>
Exports -> tsv

</details>

<details>
  <summary markdown="span">16. Save a table of the number of interactors per protein i</summary>
Exports -> tsv

</details>




