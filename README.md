# Bioinformatics in the OMICs era - a practical approach

This course aims at introducing students to computational approaches to study biological systems. Using novel experimental techniques, the functioning of biological systems can now be probed at the genome level with quantitative accuracy.  
In this course we will provide a brief refresher of key concepts to the bioinformatics and statistical aspects of large scale biological data analysis (i.e. OMICS), covered in the theoretical course (Bioinformatics in the OMICs era).
For each topic we will propose one or more exercises aimed at deepening the understanding of those critical concepts by a hands on experience on chosen datasets. 


## Program

| Theorical course    | Practical | 
|-----------|--------------------|
| Introduction: Computational biology and OMICs |   
| Sequencing technologies & sequence comparison | [BLAST](1. Monday/BLAST.md)  
| Genome assembly  | [Genome assembly](2. Tuesday/GenomeAssembly.md) 
| Genome  annotation | [Genome annotation](2. Tuesday/GenomeAnnotation.md) 
| RNAseq: from experimental design to quantification | [RNAseq data preprocessing](3. Wednesday/RNAseq_quantification.md)  |
| RNAseq: differential analysis | [RNAseq differential analysis](3. Wednesday/RNAseq_differentialAnalysis.md)  |
| Databases & functional annotation: Key bioiformatics concepts and resources | [Exploring BV-BRC](4. Thursday/Exploring_BVBRC.md) |
| Databases & functional annotation  | [Annotation using Gene Ontology and Kegg](4. Thursday/Functional_Annotation.Rmd)  |
| Functional analysis: from gene by gene to gene set enrichment  (GSEA)| [Functional analysis](4. Thursday/practicals_GSEA.Rmd) |
| Introduction to biological networks  |  |



## Authors and trainers
- Natalia Pietrosemoli: <natalia.pietrosemoli@pasteur.fr> 
- Claudia Chica: <claudia.chica@pasteur.fr> 

## License
Any use of the current material must include proper citation of authors.   

## Aim
This project's sole aim and use is for the hands-on course organised with the Universidad de La Sabana (Colombia) and in the context of the project “Implementation of biotechnological tools and recirculation systems to achieve the sustainability of tilapia farming as a productive strategy for food security and safety in La Guajira – Call 6 of the General Royalties System”. 


